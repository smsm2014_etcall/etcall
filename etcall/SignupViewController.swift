//
//  SignupViewController.swift
//  etcall
//
//  Created by Smsm on ١١ شعبان، ١٤٣٧ هـ.
//  Copyright © ١٤٣٧ هـ etcall. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        //case 5:
          //  return 2
        default:
            return 1
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = UITableViewCell()
            if indexPath.row == 0 {
                cell.textLabel?.text = "Refugee"
            }
            else if indexPath.row == 1 {
                cell.textLabel?.text = "Volunteer"
            }
            return cell
        }
        else if indexPath.section == 1 {
            let cell:EditableTableViewCell = tableView.dequeueReusableCellWithIdentifier("editableCell", forIndexPath: indexPath) as! EditableTableViewCell
            
            cell.editableTextField.placeholder = "First Name"
            return cell
        }
        else if indexPath.section == 2 {
            let cell:EditableTableViewCell = tableView.dequeueReusableCellWithIdentifier("editableCell", forIndexPath: indexPath) as! EditableTableViewCell
            
            cell.editableTextField.placeholder = "Last Name"
            return cell
        }
        else if indexPath.section == 3 {
            let cell:EditableTableViewCell = tableView.dequeueReusableCellWithIdentifier("editableCell", forIndexPath: indexPath) as! EditableTableViewCell
            
            cell.editableTextField.placeholder = "Email"
            return cell
        }
        else if indexPath.section == 4 {
            let cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "cell1")
            cell.textLabel?.text = "Section 4"
            cell.detailTextLabel?.text = "Detail text"
            return cell
        }
        else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCellWithIdentifier("booleanCell", forIndexPath: indexPath)
            let switchButton:UISwitch =  cell.viewWithTag(1) as! UISwitch
            switchButton.setOn(false, animated: true)
            cell.textLabel?.text = "Are you doctor?"
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Register as:"
        }
        else if section == 1 {
            return "First Name:"
        }
        else if section == 2 {
            return "Last Name:"
        }
        else if section == 3 {
            return "Email:"
        }
        else if section == 4 {
            return "City:"
        }
//        else if section == 5 {
//            return
//        }
        return ""
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
