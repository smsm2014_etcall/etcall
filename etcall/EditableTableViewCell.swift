//
//  EditableTableViewCell.swift
//  etcall
//
//  Created by Smsm on ١١ شعبان، ١٤٣٧ هـ.
//  Copyright © ١٤٣٧ هـ etcall. All rights reserved.
//

import UIKit

class EditableTableViewCell: UITableViewCell {

    @IBOutlet weak var editableTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
